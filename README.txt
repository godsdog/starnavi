sudo apt-get install python3-dev libmysqlclient-dev

CREATE DATABASE starnavi_test CHARACTER SET utf8;
create user 'db_user'@'localhost' identified by 'db_pass';
GRANT ALL ON starnavi_test.* TO 'db_user'@'localhost';

just copy examle_db_cred.cnf to db_cred.cnf
